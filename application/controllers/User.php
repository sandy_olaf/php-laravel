<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index()
	{
		$this->load->model('M_user');
		$data['alluser'] = $this->M_user->getAllUser();
		$data['getid'] = $this->M_user->getGanjilGenap();
		$this->load->view('User',$data);
	}
	public function add_user() {
		$this->load->model('M_user');
		$this->M_user->addDataUser();
	}
}
