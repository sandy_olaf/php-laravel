<?php

class M_user extends CI_model {

	public function getAllUser()
	{
		return $this->db->get('user')->result_array();
	}
	public function getGanjilGenap()
	{
		return $this->db->query('SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = "test_erloom" AND TABLE_NAME = "user"')->row_array();
	}
	public function addDataUser()
	{
		$next_ai = $this->input->post('post_id'); //get hidden input from form method post
		$cari_gg = $next_ai % 2; //get ganjil genap

		if($cari_gg == 0) {
			$parity = 'Odd';
		} else {
			$parity = 'Even';
		}

		$add = [
			"name" => $this->input->post('name', true),
			"parity" => $parity
		];

		$this->db->insert('user', $add);
		redirect('user');
	}

}
?>